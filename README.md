# Symbolic Observation Graph-Based Generation of Test Paths

`sogMBT` generates the observable paths from a Petri net model. It also
generates the abstract paths.

## Requirements

- `gcc >= 9.3.0`.
- `cmake`
- `flex`
- `bison`

## Build

The tool can be compiled as follows:

```bash
mkdir build
cmake -S . -B build -G Ninja         # configures the project
cmake --build build --target all -j  # builds the project using all available CPU cores
cmake --install build                # installs the project: ./assets/optlp
```

## Run

Once installed, the binary `sogMBT` will be in the `assets` folder. That is:

1. `cd ..` # if you are in the `build` folder
2. `cd assets`
3. `./sogMBT`

## Usage

```
λ> ./sogMBT --help
sogMBT: Symbolic Observation Graph-Based Generation of Test Paths
Usage: ./sogMBT [OPTIONS]

Options:
  -h,--help                   Print this help message and exit
  --input-net Path:FILE REQUIRED
                              Petri net file
  --output-folder Path:DIR REQUIRED
                              output folder
  --obs-file Path:FILE        Cover observable transitions from file [default: all transitions]
```
