#ifndef SOG_H_
#define SOG_H_

#include <vector>

#include "aggregate.hpp"

// definition of types
typedef std::vector<Aggregate *> Aggregates;

class SOG {
 public:
  /**
   * Constructor
   */
  SOG() = default;

  /**
   * Destructor
   */
  ~SOG() = default;

  /**
   * States of the graph
   */
  Aggregates states;

  /**
   * Initial state of the graph
   */
  Aggregate *initial_state{nullptr};

  /**
   * Number of states of the graph
   */
  size_t nb_states{0};

  /**
   * Number of edges of the graph
   */
  size_t nb_edges{0};

  /**
   * Set the initial state of this graph
   * @param state the initial state
   */
  void set_initial_state(Aggregate *state);

  /**
   * Print successors of an state
   * @param state state to print its successors
   */
  void PrintSuccessors(const Aggregate *state) const;

  /**
   * Add an arc to the graph
   * @param from source state
   * @param to  target state
   * @param t  transition identifier
   */
  void AddArc(Aggregate *from, Aggregate *to, int t);

  /**
   * Print predecessors of an state
   * @param state state to print its predecessors
   */
  void PrintPredecessors(const Aggregate *state) const;

  /**
   * Find a state in the graph
   * @param state the state to find
   * @return the state if found, nullptr otherwise
   */
  Aggregate *FindState(const Aggregate *state) const;

  /**
   * Set the visit flag of all states to false
   * @param state the state to start with
   * @param counter the counter of reset states
   */
  void ResetVisitedFlag(Aggregate *state, size_t counter);

  /**
   * Returns the number of BDD nodes of an state and its successors
   * @param state the state to start with
   * @param counter the counter of visited states
   * @return the number of BDD nodes
   */
  int NbBddNode(Aggregate *state, size_t counter);

  /**
   * Add an state to the graph
   * @param state the state to insert
   */
  void AddState(Aggregate *state);

  /**
   * Print the information of the graph
   */
  void PrintCompleteInformation();

  /**
   * Print the graph
   * @param state the state to start with
   * @param counter the counter of visited states
   */
  void PrintGraph(Aggregate *state, size_t counter);

  /**
   * Export graph to graphviz file
   * @param filename the name of the file
   */
  void ExportGraphToDotFile(const std::string &filename) const;
};

#endif  // SOG_H_
