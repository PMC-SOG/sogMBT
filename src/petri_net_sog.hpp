#ifndef PETRI_NET_SOG_H_
#define PETRI_NET_SOG_H_
#include <stack>
#include <vector>

#include "Net.hpp"
#include "aggregate.hpp"
#include "bdd.h"
#include "sog.hpp"

// type definitions
typedef std::vector<int> Path;
typedef std::set<Path> Paths;
typedef std::pair<Aggregate*, bdd> AggrPair;

struct StackElt {
  Aggregate* aggregate;  // pair of aggregate and the bdd
  Set firable_trans;     // set of firable transitions

  StackElt(Aggregate* p, Set s) : aggregate(p), firable_trans(std::move(s)) {}
};
typedef std::stack<StackElt> Stack;

class Trans {
 public:
  Trans(const bdd& var, bddPair* pair, const bdd& rel, const bdd& prerel,
        const bdd& precond, const bdd& postcond);

  /**
   * Forward firing
   * @param op
   * @return
   */
  bdd operator()(const bdd& op) const;

  /**
   * Backward firing
   * @param op
   * @return
   */
  bdd operator[](const bdd& op) const;

  bdd var;
  bddPair* pairs_table;
  bdd precond;
  bdd postcond;
  bdd postrel;
  bdd prerel;
};

class PetriNetSOG {
 private:
  // number of places of the petri net model
  size_t nb_places;

  // transitions of the petri net model
  std::vector<Transition> transitions;

  // Set of observable transitions of the petri net model
  Set observables;

  // Set of non observable transitions of the petri net model
  Set non_observables;

  // Mapping from transition names to their identifiers
  std::map<std::string, int> transitions_names;

  // initial marking
  bdd m0;

  // SOG transitions
  std::vector<Trans> relation;

 public:
  /**
   * Constructor
   * @param obs_trans observable transitions
   * @param non_obs_trans  non observable transitions
   * @param bound bound of the model's places (i.e., max number of tokens)
   * @param init flag to initialize the bdd
   *
   */
  PetriNetSOG(const net&, const map<int, int>& obs_trans, Set non_obs_trans,
              int bound = 32, bool init = true);

  /**
   * Destructor
   */
  ~PetriNetSOG() = default;

  /**
   * Transitive closure on unobserved transitions
   * @param from marking from which the closure is computed
   * @return the set of reached states
   */
  bdd AccessibleEpsilon(const bdd& from) const;

  /**
   *
   * @param from marking from which the backward step is computed
   * @param aggr aggregate from which the backward step is computed
   * @return
   */
  std::pair<int, bdd> StepBackward(const bdd& from,
                                   const Aggregate* aggr) const;

  /**
   * Returns the successor of a state from a transition
   * @param from state from which the successor is computed
   * @param t transition identifier
   * @return the successor state
   */
  bdd GetSuccessor(const bdd& from, int t) const;

  /**
   * Return the set of observable transitions that are firable from a state
   * @param from state from which the firable transitions are computed
   * @return the set of firable transitions
   */
  Set FirableObservableTrans(const bdd& from) const;

  /**
   * Generate the SOG graph
   * @param [out] sog SOG graph
   */
  void GenerateSOG(SOG& sog) const;

  /**
   * Compute the aggregates traversed along the observable path, each
   * associated with the set of its input states (i.e., the set of states
   * reached by the previous observable transition in the trace starting from
   * the previous aggregate).
   * @param path observable path
   * @param sog SOG graph
   * @return stack of aggregates and their input states
   */
  std::stack<AggrPair> SearchEntryPoints(Path path, const SOG& sog) const;

  /**
   * Compute the exit points of a transition from a marking
   * @param from marking from which the exit points are computed
   * @param t transition identifier
   * @return the set of exit points
   */
  bdd SearchExitPoints(const bdd& from, int t) const;

  /**
   * Returns the shortest unobservable path linking the entry and exit points of
   * the aggregate
   * @param source entry points
   * @param target exit points
   * @param aggr aggregate
   * @return unobservable path
   */
  Path SubPathAggregate(bdd* source, const bdd& target,
                        const Aggregate* aggr) const;

  /**
   * Extract observable paths. It constructs the SOG of the petri net model
   * @param sog SOG graph
   * @param trans_obs observable transitions
   * @return set of observable paths
   */
  Paths ObservablePaths(SOG& sog, std::map<int, int> trans_obs) const;

  /**
   * Extract the abstract path from an observable path
   * @param path observable path
   * @param sog built SOG
   * @return abstract path
   */
  Path AbstractPath(Path path, const SOG& sog) const;
};

#endif  // PETRI_NET_SOG_H_
