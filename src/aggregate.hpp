#ifndef AGGREGATE_H_
#define AGGREGATE_H_

#include <vector>

#include "bdd.h"

// definition of types
class Aggregate;
struct Edge {
  Aggregate* state;  // pointer to an state
  int transition;    // identifier of the transition
  Edge(Aggregate* s, int t) : state(s), transition(t) {}  // constructor
};
typedef std::vector<Edge> Edges;

class Aggregate {
 public:
  /**
   * Constructor
   */
  Aggregate() = default;

  /**
   * Destructor
   */
  ~Aggregate() = default;

  /**
   * BDD of the aggregate
   */
  bdd state;

  /*
   * Matrix representing the weights of the shortest paths inside the aggregate
   */
  std::vector<std::vector<int>> weight;

  /**
   * Flag to indicate if the state loops using epsilon transitions
   */
  bool loop{false};

  /**
   * Flag to indicate if there exists a blocking state
   */
  bool blocking{false};

  /**
   * Flag to indicate if the state was visited
   */
  bool visited{false};

  /**
   * Set of predecessors edges
   */
  Edges predecessors;

  /**
   * Set of successors edges
   */
  Edges successors;
};

#endif  // AGGREGATE_H_
