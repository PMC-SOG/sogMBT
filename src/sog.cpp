
#include "sog.hpp"

#include <fstream>

// Table with the BDD nodes of the graph
bdd *bdd_tab;

void SOG::set_initial_state(Aggregate *state) {
  initial_state = state;
}

Aggregate *SOG::FindState(const Aggregate *state) const {
  for (auto *aggr : states) {
    if (state->state.id() == aggr->state.id()) {
      return aggr;
    }
  }
  return nullptr;
}

void SOG::AddState(Aggregate *state) {
  state->visited = false;
  this->states.push_back(state);
  nb_states++;
}

int SOG::NbBddNode(Aggregate *state, size_t counter) {
  if (!state->visited) {
    bdd_tab[counter - 1] = state->state;
    state->visited = true;
    int nb_bdd = bdd_nodecount(state->state);
    for (const auto &successor : state->successors) {
      if (!successor.state->visited) {
        counter++;
        nb_bdd += NbBddNode(successor.state, counter);
      }
    }
    return nb_bdd;
  }

  return 0;
}

void SOG::PrintCompleteInformation() {
  std::cout << "\n\nGRAPH SIZE :";
  std::cout << "\n\tNB NODES : " << nb_states;
  std::cout << "\n\tNB ARCS : " << nb_edges << "\n";

  bdd_tab = new bdd[static_cast<int>(nb_states)];

  NbBddNode(initial_state, 1);
  std::cout << "\tNB BDD NODES : "
            << bdd_anodecount(bdd_tab, static_cast<int>(nb_states)) << "\n";
  ResetVisitedFlag(initial_state, 1);
}

void SOG::ResetVisitedFlag(Aggregate *state, size_t counter) {
  if (counter <= nb_states) {
    state->visited = false;
    for (const auto &successor : state->successors) {
      if (successor.state->visited) {
        counter++;
        ResetVisitedFlag(successor.state, counter);
      }
    }
  }
}

void SOG::PrintGraph(Aggregate *state, size_t counter) {
  if (counter <= nb_states) {
    std::cout << "\nSTATE NUMBER " << counter << " : \n";
    state->visited = true;
    PrintSuccessors(state);
    getchar();
    PrintPredecessors(state);
    getchar();

    for (const auto &successor : state->successors) {
      if (!successor.state->visited) {
        counter++;
        PrintGraph(successor.state, counter);
      }
    }
  }
}

void SOG::ExportGraphToDotFile(const std::string &filename) const {
  // Open file for writing
  std::ofstream file(filename);

  // Check if the file opened successfully
  if (!file.is_open()) {
    std::cerr << "Error: Could not open file for writing.\n";
    return;
  }

  file << "digraph reachab_graph {" << '\n';

  // Add the initial node (start point)
  file << "  start [shape=point, width=0];\n\n";
  file << "  start -> ag_" << initial_state->state.id() << ";\n";

  for (const auto *aggr : states) {
    for (const auto &edge : aggr->successors) {
      file << "  ag_" << aggr->state.id() << " -> "
           << "ag_" << edge.state->state.id() << " [ label = \"t"
           << edge.transition + 1 << "\"];" << '\n';
    }
  }

  file << "}\n";
  file.close();
}

void SOG::PrintSuccessors(const Aggregate *state) const {
  std::cout << bddtable << state->state << '\n';
  if (state->loop) {
    std::cout << "\n\tLooping with epsilon transitions\n";
  }

  if (state->blocking) {
    std::cout << "\n\tExisting a blocking state\n";
  }

  std::cout << "\n\tIts successors are ( " << state->successors.size()
            << " ):\n\n";
  getchar();

  for (const auto &successor : state->successors) {
    std::cout << " \t- t" << successor.transition << " -> " << bddtable
              << successor.state->state << '\n';
    getchar();
  }
}

void SOG::AddArc(Aggregate *from, Aggregate *to, const int t) {
  from->successors.insert(from->successors.begin(), Edge(to, t));
  to->predecessors.insert(to->predecessors.begin(), Edge(from, t));
  nb_edges++;
}

void SOG::PrintPredecessors(const Aggregate *state) const {
  std::cout << "\n\tIts predecessors are ( " << state->predecessors.size()
            << " ):\n\n";
  getchar();

  for (const auto &predecessor : state->predecessors) {
    std::cout << " \t- t" << predecessor.transition << " ->" << bddtable
              << predecessor.state->state << '\n';
    getchar();
  }
}
