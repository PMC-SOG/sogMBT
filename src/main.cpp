#include <CLI11.hpp>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <regex>
#include <string>

#include "Net.hpp"
#include "bdd.h"
#include "petri_net_sog.hpp"

// BDD initial values
constexpr int BDD_INITIAL_NUM_NODES = 10000;
constexpr int BDD_SIZE_CACHES = 10000;

/**
 * Return the current time in seconds
 * @return time in seconds
 */
double GetTime() {
  return static_cast<double>(clock()) / static_cast<double>(CLOCKS_PER_SEC);
}

/**
 * Check if a string is a valid transition name
 * @param str transition name
 * @return  true if the transition name is valid, false otherwise
 */
bool ValidTransitionName(const std::string& str) {
  const std::regex pattern("^t\\d+$");
  return std::regex_match(str, pattern);
}

/**
 * Save in a file the information about observable paths
 * @param output_file Path to the file where the output will be saved
 * @param obs_paths Set of observable paths
 */
void SaveObservablePaths(const Paths& obs_paths, const string& output_file) {
  cout << "\nSaving results in " << output_file << '\n';

  ofstream my_file(output_file);
  my_file << "# paths: " << obs_paths.size() << '\n';

  int path_id = 1;
  for (const vector<int>& path : obs_paths) {
    my_file << "path #" << path_id << ": " << path.size() << " transitions"
            << '\n';
    path_id++;
  }
}

/**
 * Print statistics about the generated abstract paths
 * @param abstract_paths Set of abstract paths
 */
void PrintStats(const Paths& abstract_paths) {
  size_t sum_transitions = 0;
  set<int> transitions;

  for (const Path& path : abstract_paths) {
    for (int transition_id : path) {
      transitions.insert(transition_id);
    }
    sum_transitions += path.size();
  }

  const auto nb_paths = static_cast<double>(abstract_paths.size());
  const double average = static_cast<double>(sum_transitions) / nb_paths;

  double std_deviation = 0;
  for (const Path& path : abstract_paths) {
    std_deviation += pow(path.size() - average, 2);
  }
  std_deviation = sqrt(std_deviation / nb_paths);

  cout << "# abstract paths: " << nb_paths << '\n';
  cout << "average # of transitions per abstract path: " << average << '\n';
  cout << "standard deviation: " << std_deviation << '\n';
  cout << "# covered transitions: " << transitions.size() << '\n';
}

/**
 * Prints the output of the tool
 * @param model Petri net model
 * @param sog SOG graph
 * @param obs_transitions observable transitions
 * @param obs_paths set of observable paths
 * @param abstract_paths set of abstract paths
 * @param output_file_prefix file where the information will be saved
 */
void PrintOutput(const net& model, SOG& sog,
                 const map<int, int>& obs_transitions, const Paths& obs_paths,
                 const Paths& abstract_paths,
                 const string& output_file_prefix) {
  // print SOG information
  sog.PrintCompleteInformation();
  cout << "\n# transition: " << model.transitions.size() << '\n';
  cout << "# places: " << model.places.size() << '\n';
  cout << "# observable transitions: " << obs_transitions.size() << "\n\n";

  // print stats
  PrintStats(abstract_paths);

  // save observable paths in the folder
  SaveObservablePaths(obs_paths, output_file_prefix + "_obs_paths.txt");
}

/**
 * Returns the name of the model from the filename
 * @param filename Path to the petri net model
 * @return name of the model
 */
string GetModelName(const string& filename) {
  const auto end_pos = filename.find(".n") + 1;
  const auto start_pos = filename.rfind('/') + 1;
  string name = filename.substr(start_pos, end_pos - start_pos - 1);
  return name;
}

/**
 * Load a petri net from a .net file
 * @param filename Path to the petri net model
 * @return Petri net model
 */
net LoadPetriNet(const string& filename) {
  cout << "Parsing net: " << filename << " ... ";
  net model(filename.c_str());
  cout << "done\n";

  return model;
}

/**
 * Load observable transitions from a file
 * @param model Petri net model
 * @param file Path to the file with the observable transitions
 * @param obs_trans set of observable transitions
 * @param unobs_trans set of unobservable transitions
 */
void LoadTransitions(const net& model, const string& file,
                     map<int, int>& obs_trans, set<int>& unobs_trans) {
  ifstream my_file(file);

  if (my_file) {
    string line;
    while (getline(my_file, line)) {
      // check if the transition name is valid
      if (!ValidTransitionName(line)) {
        cerr << "Error: Invalid transition name: " << line << '\n';
        exit(1);
      }

      // for a transition t1, we take the 1 as the number of occurrences
      int trans_id = stoi(line.substr(1));
      obs_trans.insert({trans_id, 1});
    }
  }

  // compute the unobservable transitions
  for (auto t = 0; t < model.transitions.size(); t++) {
    if ((obs_trans.find(t)) == obs_trans.end()) {
      unobs_trans.insert(t);
    }
  }
}

/**
 * Find the observable transitions needed for covering all the model
 * @param[in] model Petri net model
 * @param[out] obs_trans set of observable transitions
 * @param[out] unobs_trans set of unobservable transitions
 */
void FindObservableTransitions(net& model, map<int, int>& obs_trans,
                               set<int>& unobs_trans) {
  // compute the unobservable transitions of the model using the pattern
  unobs_trans = model.calcul1();

  // compute the observable transitions
  for (auto t = 0; t < model.transitions.size(); t++) {
    if ((unobs_trans.find(t)) == unobs_trans.end()) {
      obs_trans.insert({t, 1});
    }
  }
}

/**
 * Generate abstract paths of a Petri net
 * @param net_file Path to file of the petri net model
 * @param bound SOG bound (i.e., maximum number of tokens in a place)
 * @param only_sog Flag to generate only the SOG graph
 * @param transitions_file Path to file of observable transitions
 * @param output_folder Path to folder where output files will be saved
 */
void ComputeAbstractPaths(const string& net_file, int bound, bool only_sog,
                          const string& transitions_file,
                          const string& output_folder) {
  SOG sog;
  Paths obs_paths;
  Paths abstract_paths;
  set<int> unobs_trans;
  map<int, int> obs_trans;

  // output file
  string model_name = GetModelName(net_file);
  string output_file_prefix = output_folder + "/" + model_name;

  // load the petri net model
  net model = LoadPetriNet(net_file);

  // BDD initialization.
  // See https://buddy.sourceforge.net/manual/group__kernel.html
  bdd_init(BDD_INITIAL_NUM_NODES, BDD_SIZE_CACHES);

  auto start_time = GetTime();

  // if a path with transitions is not given, then we apply the algorithm to
  // find the needed observable transitions to cover all the behaviors
  if (transitions_file.empty()) {
    cout << "\nComputing observable transitions ...";
    auto start_obs_time = GetTime();
    FindObservableTransitions(model, obs_trans, unobs_trans);
    auto obs_time = GetTime() - start_obs_time;
    cout << " done\nTime for computing observable transitions: " << obs_time
         << " seconds\n";
  } else {
    LoadTransitions(model, transitions_file, obs_trans, unobs_trans);
  }

  // build the net
  cout << "\nBuilding net ...";
  auto start_net_time = GetTime();
  PetriNetSOG pn_sog(model, obs_trans, unobs_trans, bound, false);
  auto net_time = GetTime() - start_net_time;
  cout << " done\nTime for computing the net: " << net_time << " seconds\n";

  // compute only the SOG graph
  if (only_sog) {
    cout << "\nComputing the SOG ...";
    auto start_sog_time = GetTime();
    pn_sog.GenerateSOG(sog);
    auto sog_time = GetTime() - start_sog_time;
    cout << " done\nTime for computing SOG: " << sog_time << " seconds\n";

    auto elapsed_time = GetTime() - start_time;
    cout << "\nTotal time: " << elapsed_time << " seconds\n";

    // save generate SOG
    const string sog_file = output_file_prefix + "_sog.dot";
    cout << "Saving generated SOG in " << sog_file << '\n';
    sog.ExportGraphToDotFile(sog_file);
    return;
  }

  // compute the observable paths
  cout << "\nComputing observable paths ...";
  auto start_paths_time = GetTime();
  obs_paths = pn_sog.ObservablePaths(sog, obs_trans);
  auto paths_time = GetTime() - start_paths_time;
  cout << " done\nTime for computing observable paths: " << paths_time
       << " seconds\n";

  // add abstract paths
  cout << "\nComputing abstract paths ...";
  auto start_abstract_time = GetTime();
  for (const Path& path : obs_paths) {
    abstract_paths.insert(pn_sog.AbstractPath(path, sog));
  }
  auto abstract_time = GetTime() - start_abstract_time;
  cout << " done\nTime for computing abstract paths: " << abstract_time
       << " seconds\n";

  // time for generating the paths
  auto elapsed_time = GetTime() - start_time;
  cout << "\nTotal time: " << elapsed_time << " seconds\n";

  // print output
  PrintOutput(model, sog, obs_trans, obs_paths, abstract_paths,
              output_file_prefix);
}

/******************************************************************************
 * Main function
 ******************************************************************************/
int main(const int argc, char** argv) {
  CLI::App app{
      "sogMBT: Symbolic Observation Graph-Based Generation of Test Paths"};

  string input_file;
  app.add_option("--input-net", input_file, "Petri net file")
      ->type_name("Path")
      ->required()
      ->check(CLI::ExistingFile);

  string output_folder;
  app.add_option("--output-folder", output_folder, "output folder")
      ->type_name("Path")
      ->required()
      ->check(CLI::ExistingDirectory);

  std::string obs_file;
  app.add_option("--obs-file", obs_file,
                 "Cover observable transitions from file [default: all "
                 "transitions]")
      ->type_name("Path")
      ->check(CLI::ExistingFile);

  int bound{32};
  app.add_option("--bound", bound, "SOG bound [default: 32]")
      ->capture_default_str();

  bool only_sog{false};
  app.add_flag("--only-sog", only_sog,
               "Generate only the SOG graph without computing the abstract "
               "paths [default: false]");

  // parse arguments
  CLI11_PARSE(app, argc, argv);

  ComputeAbstractPaths(input_file, bound, only_sog, obs_file, output_folder);

  return 0;
}
